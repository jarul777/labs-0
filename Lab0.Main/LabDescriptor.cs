﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    public class LabDescriptor
    {
        public static Type A = typeof(Mutant);
        public static Type B = typeof(Wodny);
        public static Type C = typeof(Lądowy);

        public static string commonMethodName = "ZwróćTyp";
    }
}
