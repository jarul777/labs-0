﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab0.Main
{
    class Mutant
    {
        private string rodzaj;

        public void setrodzaj(string rodzaj)
        {
            this.rodzaj = rodzaj;
        }

        public string getrodzaj()
        {
            return this.rodzaj;
        }

        public virtual int ZwróćTyp()
        {
            return 1;
        } 
    }
}
