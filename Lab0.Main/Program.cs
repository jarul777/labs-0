﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    class Program
    {
        static void Main(string[] args)
        {
            Mutant mut = new Wodny();

            Console.Write("Typ opisywanego mutanta (1 - mutant, 2 - lądowy, 3 - wodny): ");
            Console.Write(mut.ZwróćTyp());
            
            Console.ReadKey();
        }
    }
}
